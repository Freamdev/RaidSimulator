﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class ModifierResponse
    {
        public float[] Damages = new float[(int)DamageTypesEnum.Size];
        public bool[] MakeImmune = new bool[(int)DamageTypesEnum.Size];

        public bool[] ArrayOr(bool[] first, bool[] second)
        {
            if (first.Length != second.Length)
            {
                throw new Exception("Viki was right...!");
            }
            bool[] newArray = new bool[(int)DamageTypesEnum.Size];

            for(int i = 0; i < (int)DamageTypesEnum.Size; i++)
            {
                newArray[i] = first[i] || second[i];
            }

            return newArray;
        }
    }
}
