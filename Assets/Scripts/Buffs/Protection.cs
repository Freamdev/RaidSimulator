﻿using Assets.StringConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Buffs
{
    class Protection : Buff
    {
        public float Power { get; set; }

        public Protection(Guid id) : base(id)
        {
            Id = BuffGuids.Protection;
            Persistent = true;
        }

        public override void Effect()
        {
            throw new NotImplementedException();
        }

        public override bool IsBetter(Buff current)
        {
            var activeBuff = current as Protection;
            if(this.Power > activeBuff.Power)
            {
                return true;
            }
            return false;
        }

        public override ModifierResponse EffectDamages(ModifierResponse damages)
        {
            damages.Damages[(int)DamageTypesEnum.Physical] -= Power;
            return damages;
        }
    }
}
