﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Buffs
{
    public abstract class Buff
    {
        public bool Persistent { get; set; }
        public float Duration { get; set; }

        public Guid SourceId { get; set; }

        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">The buff generator item's Guid</param>
        public Buff(Guid id)
        {
            SourceId = id;
        }

        public abstract ModifierResponse EffectDamages(ModifierResponse damages);

        public abstract bool IsBetter(Buff current);

        public abstract void Effect();
    }
}
