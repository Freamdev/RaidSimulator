﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        var health = this.gameObject.GetComponent<Health>().health;
        if(health < 0)
        {
            this.gameObject.SetActive(false);
        }
	}
}
