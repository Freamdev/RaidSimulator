﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {

    public Texture forwardTexture;
    public Texture backTexture;

    public float maxWidth = 50;
    public float drawHeight = 8;



    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
       
    }

    void HpBackground()
    {
        DrawHpBar(maxWidth + 2, backTexture, true);
    }

    void HpForward()
    {
        float currentHp = this.GetComponent<Health>().health;
        float maxHp = this.GetComponent<Health>().maxHealth;
        float drawWidth = currentHp / maxHp * maxWidth;

        DrawHpBar(drawWidth, forwardTexture, false);
    }

    void DrawHpBar(float drawWidth, Texture texture, bool background)
    {
        var drawXYZ = Camera.main.WorldToScreenPoint(this.transform.position);
        float drawX = drawXYZ.x - 25 + (!background ? 1 : 0);
        float drawY = -drawXYZ.y + Camera.main.pixelHeight - 45 - (background ? 1 : 0);

        Rect pos = new Rect(
            drawX,
            drawY,
            drawWidth,
            drawHeight + (background ? 2 : 0)
        );

        GUI.DrawTexture(pos, texture);

    }

    void OnGUI()
    {
        HpBackground();
        HpForward();
        
    }
}
