﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Buffs;
using System;
using Assets.Scripts.Items;

public class AppliedBuffs : MonoBehaviour {

    public Dictionary<Guid, Buff> Buffs { get; set; }

	// Use this for initialization
	void Start () {
        Buffs = new Dictionary<Guid, Buff>();
	}
	
	// Update is called once per frame
	void Update () {

	}
}
