﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Items;

public class AttackHighestAgro : MonoBehaviour
{

    GameObject target { get; set; }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var agroList = GetComponent<Agro>().agro;
        target = agroList.OrderByDescending(ag => ag.Value).First().Key; ;
        var possibleWeapons = GetAllChild(this.gameObject);

        List<IWeapon> realWeapons = new List<IWeapon>();
        foreach(var pw in possibleWeapons)
        {
            var weaponComponents = pw.GetComponents<IWeapon>();
            realWeapons.AddRange(weaponComponents);
            
        }
        var weaponToUse = realWeapons.FirstOrDefault(iw => iw.GetCooldown() <= 0);
        if (weaponToUse != null)
        {
            weaponToUse.Attack(target.GetComponent<Actor>());
        }

        realWeapons.Where(iw => iw.GetCooldown() >= 0).ToList().ForEach(iw => iw.SetCooldown(iw.GetCooldown() - 1 * Time.deltaTime));
    }


    List<GameObject> GetAllChild(GameObject parent)
    {
        List<GameObject> childs = new List<GameObject>();

        foreach(Transform trans in parent.transform)
        {
            childs.AddRange(GetAllChild(trans.gameObject));
            childs.Add(trans.gameObject);
        }

        return childs;
    }
}
