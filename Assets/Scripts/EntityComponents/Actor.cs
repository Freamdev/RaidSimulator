﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Items;
using Assets.Scripts;
using UnityEngine.UI;

public class Actor : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }


    public void Attacked(IWeapon weapon)
    {
        var damages = weapon.GetDamages();
        var buffs = GetComponent<AppliedBuffs>().Buffs;

        ModifierResponse response = new ModifierResponse();
        response.Damages = damages;

        foreach (var buff in buffs)
        {
            ModifierResponse tempResponse = buff.Value.EffectDamages(response);
            response.Damages = tempResponse.Damages;
            response.MakeImmune = response.ArrayOr(response.MakeImmune, tempResponse.MakeImmune);
        }

        weapon.DamageActor(this, response);
    }

    public void Damage(float damage, int type)
    {
        GetComponent<Health>().health -= damage;
    }
}
