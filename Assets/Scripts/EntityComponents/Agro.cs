﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Assets;

public class Agro : MonoBehaviour
{

    public Dictionary<GameObject, int> agro = new Dictionary<GameObject, int>();

    // Use this for initialization
    void Start()
    {
        if(Config.Mode == "DEV")
        {
            var golem = GameObject.Find("objGraniteGolem");
            var player = GameObject.Find("PlayerCharacters").transform.GetChild(0);
            golem.GetComponent<Agro>().AddAgro(player.gameObject, 100);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void AddAgro(GameObject attacker, int amount)
    {
        if (agro.ContainsKey(attacker))
        {
            agro[attacker] += amount;
        }
        else
        {
            agro.Add(attacker, amount);
        }
    }
}
