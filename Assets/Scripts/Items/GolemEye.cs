﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Items
{
    class GolemEye : MonoBehaviour, IWeapon
    {
        public float[] Damages = new float[(int)DamageTypesEnum.Size];


        public float Cooldown = 0;
        public float MaxCooldown = 12;
        public float AttackRate = 3;

        public GolemEye()
        {
            Damages[(int)DamageTypesEnum.Physical] = 0;
            Damages[(int)DamageTypesEnum.Dark] = 20;
        }

        public void Attack(Actor target)
        {
            target.Attacked(this);
            this.Cooldown = MaxCooldown;
        }

        public void DamageActor(Actor target, ModifierResponse response)
        {
            for (int i = 0; i < (int)DamageTypesEnum.Size; i++)
            {
                if (!response.MakeImmune[i] && response.Damages[i] > 0)
                {
                    print(transform.root.name + " dealt: " + response.Damages[i] + " damage to: " + target.gameObject.name + " ");
                    target.Damage(response.Damages[i], i);
                }
            }
        }

        public float GetAttackRate()
        {
            throw new NotImplementedException();
        }

        public float GetCooldown()
        {
            return Cooldown;
        }

        public float[] GetDamages()
        {
            float[] ret = new float[(int)DamageTypesEnum.Size];
            for (int i = 0; i < (int)DamageTypesEnum.Size; i++)
            {
                ret[i] = Damages[i];
            }
            return ret;
        }

        public void SetCooldown(float val)
        {
            Cooldown = val;
        }
    }
}
