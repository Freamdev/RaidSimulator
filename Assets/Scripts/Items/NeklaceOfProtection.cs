﻿using UnityEngine;
using System.Collections;
using System;
using Assets.Scripts.Items;
using Assets.StringConstants;
using Assets.Scripts.Buffs;

public class NeklaceOfProtection : MonoBehaviour, IMonoItem
{

    public Guid Id { get; set; }

    public float Power = 3;

    // Use this for initialization
    void Start()
    {
        Id = Guid.NewGuid();
    }

    // Update is called once per frame
    void Update()
    {
        ApplyBuff();
    }

    public void Equip()
    {
        ApplyBuff();
    }

    public void ApplyBuff()
    {
        var user = transform.root.gameObject;

        var buffs = user.GetComponent<AppliedBuffs>().Buffs;

        Protection newBuff = new Protection(Id);
        newBuff.Power = this.Power;

        bool protectionAppliedAllready = buffs.ContainsKey(BuffGuids.Protection);

        if (protectionAppliedAllready)
        {
            if (newBuff.IsBetter(buffs[BuffGuids.Protection]))
            {
                buffs.Remove(BuffGuids.Protection);
                buffs.Add(BuffGuids.Protection, newBuff);
            }
            else
            {
                print("This type of protection is applied allready on the character");
            }
        }
        else
        {
            buffs.Add(BuffGuids.Protection, newBuff);
        }
    }
}
