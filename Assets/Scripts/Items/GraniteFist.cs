﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Items;
using System;
using Assets.Scripts;

public class GraniteFist : MonoBehaviour, IWeapon
{

    public float[] Damages;

    public float DamageForOwerwhelm = 5;
    public float OverwhelmDamage = 12;

    public float Cooldown = 0;
    public float MaxCooldown = 8;
    public float AttackRate = 3;

    public GraniteFist()
    {
       
    }

    public void Attack(Actor target)
    {
        target.Attacked(this);
        this.Cooldown = MaxCooldown;
    }

    public void DamageActor(Actor target, ModifierResponse response)
    {
      
        for (int i = 0; i < (int)DamageTypesEnum.Size; i++)
        {
            if (!response.MakeImmune[i] && response.Damages[i] > 0)
            {
                print(transform.root.name + " dealt: " + response.Damages[i] + " damage to: " + target.gameObject.name + " with: "+this.name);
                target.Damage(response.Damages[i], i);
            }
        }

        if (!response.MakeImmune[(int)DamageTypesEnum.Physical]
            && response.Damages[(int)DamageTypesEnum.Physical] > DamageForOwerwhelm)
        {
            print(transform.root.name + " overwhelmed " + target.gameObject.name + " for " + OverwhelmDamage + " damage.");
            target.Damage(OverwhelmDamage, (int)DamageTypesEnum.Physical);
        }
    }

    public float[] GetDamages()
    {
        float[] ret = new float[(int)DamageTypesEnum.Size];
       
        for (int i = 0; i < (int)DamageTypesEnum.Size; i++)
        {
          
            ret[i] = Damages[i];
            
        }
        return ret;
    }

    // Use this for initialization
    void Start()
    {
        Damages = new float[(int)DamageTypesEnum.Size];

        Damages[(int)DamageTypesEnum.Physical] = 5;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public float GetCooldown()
    {
        return Cooldown;
    }

    public void SetCooldown(float val)
    {
        Cooldown = val;
    }

    public float GetAttackRate()
    {
        return AttackRate;
    }
}
