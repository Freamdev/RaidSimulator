﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Items
{
    public interface IWeapon
    {
        void Attack(Actor target);

        void DamageActor(Actor target, ModifierResponse response);

        float[] GetDamages();

        float GetCooldown();

        void SetCooldown(float val);

        float GetAttackRate();
    }
}
