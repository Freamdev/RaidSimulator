﻿using UnityEngine;
using System.Collections;
using System;

public enum ItemSlotEnum
{
    Head,
    RightHand,
    LeftHand,
    Body,
    Ring,
    Earring,
    Bracelet,
    Neklace,
    Legs,
    Foot,
    Belt,
    Size
}





